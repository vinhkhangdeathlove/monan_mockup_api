import { monAnService } from "./service/monAnService.js";

import { spinnerService } from "./service/spinnerService.js";
import { monAnController } from "./controller/monAncontroller.js";

let foodList = [];

let idFoodEdited = null;

let xoaMonAn = (maMonAn) => {
  spinnerService.batLoading();
  monAnService
    .xoaMonAn(maMonAn)
    .then((res) => {
      spinnerService.tatLoading();

      renderDanhSachService();
    })
    .catch((err) => {
      spinnerService.tatLoading();
    });
};
window.xoaMonAn = xoaMonAn;

let layChiTietMonAn = (idMonAn) => {
  idFoodEdited = idMonAn;
  spinnerService.batLoading();
  monAnService
    .layThongTinChiTietMonAn(idMonAn)
    .then((res) => {
      spinnerService.tatLoading();
      monAnController.showThongTinLenForm(res.data);
    })
    .catch((err) => {
      spinnerService.tatLoading();
    });
};

window.layChiTietMonAn = layChiTietMonAn;

let renderTable = (list) => {
  let contentHTML = "";
  for (let index = 0; index < list.length; index++) {
    let monAn = list[index];
    let contentTr = `<tr> 
                         <td> ${monAn.id} </td>
                         <td> ${monAn.name} </td>
                         <td> ${monAn.price} </td>
                         <td> ${monAn.description} </td>

                         <td>    
                                  <button
                                  onclick="layChiTietMonAn(${monAn.id})"
                                  class="btn btn-primary">Sửa</button>
                                  <button
                                  onclick="xoaMonAn(${monAn.id})"
                                  class="btn btn-warning">Xoá</button>
                         </td>
                     </tr>`;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tbody_food").innerHTML = contentHTML;
};
let renderDanhSachService = () => {
  spinnerService.batLoading();
  monAnService
    .layDanhSachMonAn()
    .then((res) => {
      spinnerService.tatLoading();

      foodList = res.data;

      renderTable(foodList);
    })
    .catch((err) => {
      spinnerService.tatLoading();
    });
};

renderDanhSachService();

let themMonAn = () => {
  let monAn = monAnController.layThongTinTuForm();

  spinnerService.batLoading();
  monAnService
    .themMoiMonAn(monAn)
    .then((res) => {
      spinnerService.tatLoading();
      renderDanhSachService();
    })
    .catch((err) => {
      spinnerService.tatLoading();

      alert("thất bại");
    });
};

window.themMonAn = themMonAn;

let capNhatMonAn = () => {
  console.log("yes");
  let monAn = monAnController.layThongTinTuForm();
  console.log("monAn: ", monAn);
  spinnerService.batLoading();

  let newMonAn = { ...monAn, id: idFoodEdited };
  console.log("newMonAn: ", newMonAn);

  monAnService
    .capNhatMonAn(newMonAn)
    .then((res) => {
      spinnerService.tatLoading();
      monAnController.showThongTinLenForm({
        name: "",
        price: "",
        description: "",
      });
      renderDanhSachService();
    })
    .catch((err) => {
      spinnerService.tatLoading();
      console.log(err);
    });
};

window.capNhatMonAn = capNhatMonAn;
